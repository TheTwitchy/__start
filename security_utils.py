import jwt
import datetime
import functools
import flask

class AuthenticationException(Exception):
    pass

class AuthorizationException(Exception):
    pass

def require_login(requires_admin = False):
    if "user_id" not in flask.session:
        raise AuthenticationException
    if requires_admin == True:
        if "is_admin" not in flask.session or flask.session["is_admin"] == False:
            raise AuthorizationException
    
class ContentException(Exception):
    pass

def ensure_json(request):
    try:
        # this split is because sometimes the content type can have a charset option after it.
        ct = request.content_type.split(";")[0]
        if not ct == "application/json":
            raise ContentException
    except:
        raise ContentException