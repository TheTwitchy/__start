import os, base64
basedir = os.path.abspath(os.path.dirname(__file__))

def get_env(var):
    try:
        a = os.environ[var]
        return a
    except KeyError:
        return ""

class Development(object):
    FLASK_ENV = "development"
    DEVELOPMENT = True
    DEBUG = True
    CSRF_ENABLED = True
    SECRET_KEY = get_env("SECRET_KEY")
    SQLALCHEMY_DATABASE_URI = get_env("DATABASE_URL") 
    SESSION_COOKIE_NAME = "__token"

class Production(Development):
    FLASK_ENV = "production"
    DEBUG = False
    TESTING = False

    