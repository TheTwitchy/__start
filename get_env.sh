#!/bin/bash

rm .env
touch .env

export PORT=5000

# If you need to do development, enable this, otherwise leave it alone
# echo "__DEVELOPMENT=1" >> .env

# In general you should not need to change anything under this
echo "PORT=$PORT" >> .env

export POSTGRES_USER=start_user
echo "POSTGRES_USER=$POSTGRES_USER" >> .env
export POSTGRES_DB=startdb
echo "POSTGRES_DB=$POSTGRES_DB" >> .env

# Generate a random password for the database
array=()
for i in {a..z} {A..Z} {0..9}; do
    array[$RANDOM]=$i
done
export POSTGRES_PASSWORD=`printf %s ${array[@]::20} $'\n'`
echo "POSTGRES_PASSWORD=$POSTGRES_PASSWORD" >> .env

# Build the full database URL that the webapp uses
echo "DATABASE_URL=postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@db/$POSTGRES_DB" >> .env

# Generate a random password for the secret key
array=()
for i in {a..z} {A..Z} {0..9}; do
    array[$RANDOM]=$i
done
export SECRET_KEY=`printf %s ${array[@]::20} $'\n'`
echo "SECRET_KEY=$SECRET_KEY" >> .env
