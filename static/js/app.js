"use strict";

// Define the `start` module
angular.module("startApp", [
    // Dependencies
    "ngRoute",
    "focus-if",
    "userCreate",
    "userView",
    "usersList",
    "login",
    "setup",
    "startMain",
    "shortcutList",
    "shortcutCreate",
    "shortcutView",
    "error403",
    "error404",
    "error500",
]);

angular.
module("startApp").
config(["$routeProvider",
    function config($routeProvider) {
        $routeProvider.
        when("/", {
            template: "<start-main></start-main>"
        }).
        when("/login", {
            template: "<login></login>"
        }).
        when("/setup", {
            template: "<setup></setup>"
        }).
        when("/users", {
            template: "<users-list></users-list>"
        }).
        when("/users/new", {
            template: "<user-create></user-create>"
        }).
        when("/users/:userId", {
            template: "<user-view></user-view>"
        }).
        when("/shortcuts", {
            template: "<shortcut-list></shortcut-list>"
        }).
        when("/shortcuts/new", {
            template: "<shortcut-create></shortcut-create>"
        }).
        when("/shortcuts/:shortcutId", {
            template: "<shortcut-view></shortcut-view>"
        }).
        when("/error/permission", {
            template: "<error-403></error-403>"
        }).
        when("/error/not-found", {
            template: "<error-404></error-404>"
        }).
        when("/error/unknown", {
            template: "<error-500></error-500>"
        }).
        otherwise("/");
    }
]).filter('checkmark', function() {
    return function(input) {
        return input ? '\u2713' : '';
    };
});;

