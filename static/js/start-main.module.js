"use strict";

// Define the `startMain` module
angular.module("startMain", ["ngRoute"]);

// Register `startMain` component, along with its associated controller and template
angular.
module("startMain").component("startMain", {
    templateUrl: "/static/js/start-main.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function startMainController($http, $routeParams, $location, $rootScope) {
        var self = this;
        $rootScope.title = "";
        self.errors = [];

        $http.get("api/shortcuts").then(
            function(response) {
                self.shortcuts = response.data;
                self.errors = [];
                self.is_working = false;
            },
            function(error_response) {
                // Handle errors here
                self.errors = error_response.data.error_msgs;
                self.is_working = false;
            } 
        );

        self.submitKeyword = function (){
            var input = self.mainInput;
            self.mainInput = "";
            for (let n = 0; n < self.shortcuts.length; n++){
                if (self.shortcuts[n].keyword == input){
                    window.open(self.shortcuts[n].url, "_blank");
                    return;
                }
            }
            window.open("https://www.ecosia.org/search?q=" + input, "_blank");
        };
    }]
});