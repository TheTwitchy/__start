"use strict";

// Define the `login` module
angular.module("login", ["ngRoute"]);

// Register `userView` component, along with its associated controller and template
angular.
module("login").component("login", {
    templateUrl: "/static/js/login.template.html",
    controller: ["$http", "$routeParams", "$rootScope", "$location", function LoginController($http, $routeParams, $rootScope, $location) {
        var self = this;
        $rootScope.title = " - Login";
        self.is_working = false;
        self.errors = [];
        self.next = $routeParams.next;

        this.login = function (){
            self.errors = [];
            self.is_working = true;
            $http.post("/api/login", self.loginForm).then(
                function(response) {
                    $location.path("/");
                    self.errors = [];
                    
                }, 
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                }
            );
        };    
    }]
});