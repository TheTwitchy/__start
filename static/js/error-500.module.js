"use strict";

// Define the `error500` module
angular.module("error500", ["ngRoute"]);

// Register `error500` component, along with its associated controller and template
angular.
module("error500").component("error500", {
    templateUrl: "/static/js/error-500.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function error500Controller($http, $routeParams, $location, $rootScope) {
        var self = this;
        $rootScope.title = " - Error";

        self.goHome = function (){
            $location.path("/");
        };
    }]
});