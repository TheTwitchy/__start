"use strict";

// Define the `userCreate` module
angular.module("userCreate", ["ngRoute"]);

// Register `userCreate` component, along with its associated controller and template
angular.
module("userCreate").component("userCreate", {
    templateUrl: "/static/js/user-create.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function userCreateController($http, $routeParams, $location, $rootScope) {
        var self = this;
        $rootScope.title = " - New User";
        self.is_working = false;
        self.errors = [];

        // This weird setting is here because for some reason the is_admin checkbox doesn't get included in the object scope.
        self.newuser = {};
        self.newuser.is_admin = false;

        self.createUserSubmit = function (){
            self.errors = [];
            self.is_working = true;
            $http.post("/api/users", self.newuser).then(
                function(response) {
                    // Handle success case here
                    $location.path("/users/" + response.data.id);
                    self.errors = [];
                }, 
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                }
            );
        };

        this.goList = function (){
            self.errors = [];
            self.is_working = true;
            $location.path("/users");
        };
    }]
});