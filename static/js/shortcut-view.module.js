"use strict";

// Define the `shortcutView` module
angular.module("shortcutView", ["ngRoute"]);

// Register `shortcutView` component, along with its associated controller and template
angular.
module("shortcutView").component("shortcutView", {
    templateUrl: "/static/js/shortcut-view.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function shortcutViewController($http, $routeParams, $location, $rootScope) {
        var self = this;
        $rootScope.title = " - Shortcut";
        self.is_working = true;
        self.errors = [];

        $http.get("api/shortcuts/" + $routeParams.shortcutId).then(
            function(response) {
                self.shortcut = response.data;
                self.errors = [];
                self.is_working = false;
            },
            function(error_response) {
                // Handle errors here
                self.errors = error_response.data.error_msgs;
                self.is_working = false;
            } 
        );

        self.editShortcutSubmit = function (){
            self.is_working = true;
            $http.put("/api/shortcuts/" + $routeParams.shortcutId, self.shortcut).then(
                function(response) {
                    self.errors = [];
                    self.is_working = false;
                },
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                } 
            );
        };

        self.goList = function (){
            self.errors = [];
            $location.path("/shortcuts");
        };

        self.deleteUserSubmit = function (){
            self.is_working = true;
            $http.delete("/api/shortcuts/" + $routeParams.shortcutId).then(
                function(response) {
                    self.errors = [];
                    $location.path("/shortcuts");
                },
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                } 
            );
        };
    }]
});