"use strict";

// Define the `setup` module
angular.module("setup", ["ngRoute"]);

// Register `userView` component, along with its associated controller and template
angular.
module("setup").component("setup", {
    templateUrl: "/static/js/setup.template.html",
    controller: ["$http", "$location", "$rootScope", function SetupController($http, $location, $rootScope) {
        var self = this;
        $rootScope.title = " - Setup";
        self.is_working = false;
        self.errors = [];

        this.setup = function (){
            self.errors = [];
            self.is_working = true;
            $http.post("/api/setup", self.newuser).then(
                function(response) {
                    // Handle success case here
                    $location.path("/");
                    self.errors = [];
                }, 
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                }
            );
        };    
    }]
});