"use strict";

// Define the `usersList` module
angular.module("usersList", ["ngRoute"]);

// Register `userView` component, along with its associated controller and template
angular.
module("usersList").component("usersList", {
    templateUrl: "/static/js/users-list.template.html",
    controller: function UsersListController($http, $rootScope) {
        var self = this;
        $rootScope.title = " - Users";
        self.is_working = true;
        self.errors = [];        

        $http.get("api/users").then(
            function(response) {
                self.users = response.data;
                self.errors = [];
                self.is_working = false;
            },
            function(error_response) {
                // Handle errors here
                self.errors = error_response.data.error_msgs;
                self.is_working = false;
            } 
        );
    }
});