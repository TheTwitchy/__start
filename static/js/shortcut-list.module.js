"use strict";

// Define the `shortcutList` module
angular.module("shortcutList", ["ngRoute"]);

// Register `shortcutList` component, along with its associated controller and template
angular.
module("shortcutList").component("shortcutList", {
    templateUrl: "/static/js/shortcut-list.template.html",
    controller: function shortcutListController($http, $rootScope) {
        var self = this;
        $rootScope.title = " - Shortcuts";
        self.is_working = true;
        self.errors = [];

        $http.get("api/shortcuts").then(
            function(response) {
                self.shortcuts = response.data;
                self.errors = [];
                self.is_working = false;
            },
            function(error_response) {
                // Handle errors here
                self.errors = error_response.data.error_msgs;
                self.is_working = false;
            } 
        );
    }
});