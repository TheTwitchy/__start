"use strict";

// Define the `shortcutCreate` module
angular.module("shortcutCreate", ["ngRoute"]);

// Register `shortcutCreate` component, along with its associated controller and template
angular.
module("shortcutCreate").component("shortcutCreate", {
    templateUrl: "/static/js/shortcut-create.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function shortcutCreateController($http, $routeParams, $location, $rootScope) {
        var self = this;
        $rootScope.title = " - New Shortcut";
        self.is_working = false;
        self.errors = [];

        self.newshortcut = {};

        self.createShortcutSubmit = function (){
            self.is_working = true;
            $http.post("/api/shortcuts", self.newshortcut).then(
                function(response) {
                    // Handle success case here
                    $location.path("/shortcuts/" + response.data.id);
                    self.errors = [];
                }, 
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                }
            );
        };

        self.goList = function (){
            $location.path("/shortcuts");
        };
    }]
});