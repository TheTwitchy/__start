# \_\_start
Underscore service start page. Provides a bookmark-like way to access frequently used sites, and just performs a Ecosia search for everything else.

## Screenshots
See the [wiki](https://gitlab.com/TheTwitchy/__start/-/wikis/home).

## Requirements
* Docker server
* Docker Compose
* A domain name

## Installation
### Production
We recommend you use Docker Compose for automated deployment. Manual deployment without Docker is possible, but more painful.

#### Automated Deployment 
* `get_env.sh`
* `docker-compose up -d`

### Development
#### VirtualENV
* `pip install virtualenv`
* `virtualenv venv`

#### Install Dependencies
* `source venv/bin/activate`
* `pip install -r requirements.txt`

#### Setup Postgresql
* `sudo apt install postgresql`
* `sudo su postgres`
* `psql`
    * `CREATE DATABASE __start_dev;` 
    * `CREATE USER __user_dev WITH ENCRYPTED PASSWORD 'enter_password_here';`
    * `GRANT ALL PRIVILEGES ON DATABASE __start_dev TO __user_dev;`
    * `\q`
* `python manage.py db init`

##### Updates Models in Database
* `python manage.py db migrate`
    * Only needs to be run on updates to model.py.
* `python manage.py db upgrade`
    * Run this to update the layout of your DB when first start development and whenever model changes are made.

#### Run Development HTTP Server
* `source dev_env && python manage.py runserver`
* Then visit [the development server](http://localhost1.dev.thetwitchy.com:5000/). For an explanation of why this URL is used, see config.py in the \_\_users repo.