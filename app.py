import flask 
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
import json
import urllib.parse
from flask_bcrypt import Bcrypt
from flask_migrate import Migrate

from security_utils import AuthenticationException, AuthorizationException, require_login, ContentException, ensure_json

class StartServiceException(Exception):
    pass

app = Flask(__name__)
try:
    if not os.environ["__DEVELOPMENT"] == None :
        app.config.from_object("config.Development")
        app.logger.warning("Starting __start app in DEVELOPMENT mode. This has some security implications, so if this is unexpected, QUIT NOW.")
except:
    app.config.from_object("config.Production")
    app.logger.info("Starting __start app in normal mode.")

if app.config["SECRET_KEY"] == b"" or app.config["SQLALCHEMY_DATABASE_URI"] == "": 
    app.logger.error("The ENV is not setup correctly. Quitting.")
    quit(-1)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)
bcrypt = Bcrypt(app)


from models import Shortcut, User

# Quick function to determine if there is any user listed as an admin in the database.
def does_admin_exist():
    users = db.session.query(User).filter_by(is_admin = True).first()
    return not users == None

# Very basic password strength checker, must be 8+ chars, mix of upper and lowercase, and contain at least one digit.
def password_is_strong(cleartext_password):
    if len(cleartext_password) < 8:
        return False
    if cleartext_password.lower() == cleartext_password:
        return False
    if cleartext_password.upper() == cleartext_password:
        return False
    if not any(char.isdigit() for char in cleartext_password):
        return False
    return True

@app.route("/")
def index():
    return flask.render_template("main.html") 

@app.route("/login", methods=["GET"])
def login():
    # If already authenticated, just go straight to user details.
    try:
        if "user_id" in flask.session:
            return flask.redirect(flask.url_for("index"))
    except KeyError:
        pass

    # Handle initial setup if no admin exists
    if not does_admin_exist():
        return flask.redirect(flask.url_for("setup"))

    return flask.redirect("/#!/login")


@app.route("/logout")
def logout():
    flask.session.pop("user_id")
    resp = flask.make_response(flask.redirect(flask.url_for("login")))
    return resp


# This endpoint will setup an initial superuser.
@app.route("/setup", methods=["GET"])
def setup():

    # Do not continue if an admin already exists
    if does_admin_exist():
        return flask.redirect(flask.url_for("login"))

    return flask.redirect("/#!/setup")

@app.route("/api/login", methods=["POST"])
def api_login():
    # I realize it's a bit stupid to add this as a CSRF protection when the user isn't authenticated yet, but shut up.
    try:
        ensure_json(flask.request)
    except ContentException:
        d = {}
        d["error_msgs"] = ["Incorrect content type."]
        resp = flask.Response(json.dumps(d), status = 400)
        resp.mimetype = "application/json"
        return resp

    d = {}
    d["error_msgs"] = []

    next_target = ""

    login_dict = flask.request.get_json()

    email = login_dict["email"]
    cleartext_password = login_dict["password"]
    
    user = db.session.query(User).filter_by(email = email).first()

    if not user:
        # User does not exist
        d["error_msgs"].append("Login failed. Check your username and password.")
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    elif not user.is_correct_password(cleartext_password):
        # User exists, but entered password is incorrect
        d["error_msgs"].append("Login failed. Check your username and password.")
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    else:
        # User authenticated successfully
        tmp_obj = {}
        resp = flask.Response(json.dumps(tmp_obj), status = 200)
        flask.session["user_id"] = user.id
        flask.session["is_admin"] = user.is_admin
        flask.session["email"] = user.email
        resp.set_cookie("__theme", user.theme, max_age=60*60*24*365, httponly=False, secure=flask.request.scheme == "https")
        resp.mimetype = "application/json"
        return resp

    
# This endpoint will setup an initial superuser.
@app.route("/api/setup", methods=["POST"])
def api_setup():
    # I realize it's a bit stupid to add this as a CSRF protection when the user isn't authenticated yet, but shut up.
    try:
        ensure_json(flask.request)
    except ContentException:
        d = {}
        d["error_msgs"] = ["Incorrect content type."]
        resp = flask.Response(json.dumps(d), status = 400)
        resp.mimetype = "application/json"
        return resp

    d = {}
    d["error_msgs"] = []

    # Do not continue if an admin already exists
    if does_admin_exist():
        d["error_msgs"].append("Setup has already been run.")
        app.logger.warning("Setup of instance failed, administrator already exists.")
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    
    new_user_dict = flask.request.get_json()

    # Required fields.
    try:
        a = new_user_dict["email"]
        a = new_user_dict["name"]
        a = new_user_dict["password1"]
        a = new_user_dict["password2"]
    except KeyError:
        d = {}
        d["error_msgs"] = ["Administrator creation failed, missing required fields."]
        resp = flask.Response(json.dumps(d), status = 400)
        resp.mimetype = "application/json"
        return resp

    if not new_user_dict["password1"] == new_user_dict["password2"]:
        # Passwords don't match. 
        d = {}
        d["error_msgs"] = ["Administrator creation failed, passwords don't match."]
        resp = flask.Response(json.dumps(d), status = 400)
        resp.mimetype = "application/json"
        return resp

    if not password_is_strong(new_user_dict["password1"]):
        # Password isn't strong enough. 
        d = {}
        d["error_msgs"] = ["Administrator creation failed, passwords is too weak. Increase it's complexity."]
        resp = flask.Response(json.dumps(d), status = 400)
        resp.mimetype = "application/json"
        return resp

    new_user = User(email = new_user_dict["email"], full_name = new_user_dict["name"], cleartext_password = new_user_dict["password1"], is_admin = True)

    try:
        db.session.add(new_user)
        db.session.commit()
        return api_user(new_user.id)
    except:
        d = {}
        d["error_msgs"] = ["Administrator creation failed, commit to database was unsuccessful."]
        resp = flask.Response(json.dumps(d), status = 400)
        resp.mimetype = "application/json"
        return resp


# REST interface for listing all users and creating a new user. All authenticated users can access the users listing, as it's probably needed by other applications.
@app.route("/api/users", methods=["GET", "POST"])
def api_users_list():
    try:
        require_login()

    except AuthenticationException:
        d = {}
        d["error_msgs"] = ["You must be authenticated."]
        resp = flask.Response(json.dumps(d), status = 401)
        resp.mimetype = "application/json"
        return resp
    except AuthorizationException:
        d = {}
        d["error_msgs"] = ["You do not have permission to access that."]
        app.logger.warning("AuthorizationException: User '%s' tried to access the users list." % (flask.session["email"]))
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    if flask.request.method == "GET":
        users_dict = []
        user_objs = db.session.query(User).all()
        for user in user_objs:
            tmp_user_dict = user.to_dict()
            # Regular, non-admin users can only see some parts of a user's profile. This checks to see if they are an admin, and restrict the information provided if not
            try:
                require_login(requires_admin = True)
                users_dict.append(tmp_user_dict)
            except AuthorizationException:
                continue

        resp = flask.Response(json.dumps(users_dict), status = 200)
        resp.mimetype = "application/json"
        return resp

    elif flask.request.method == "POST":
        try:
            ensure_json(flask.request)
        except ContentException:
            d = {}
            d["error_msgs"] = ["Incorrect content type."]
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        # Only admins can create new users
        try:
            require_login(requires_admin = True)
        except AuthorizationException:
            d = {}
            d["error_msgs"] = ["You do not have permission to access that."]
            app.logger.warning("AuthorizationException: User '%s' tried to access the users list." % (flask.session["email"]))
            resp = flask.Response(json.dumps(d), status = 403)
            resp.mimetype = "application/json"
            return resp


        new_user_dict = flask.request.get_json()

        # Required fields.
        try:
            a = new_user_dict["email"]
            a = new_user_dict["name"]
            a = new_user_dict["password1"]
            a = new_user_dict["password2"]
        except KeyError:
            d = {}
            d["error_msgs"] = ["User creation failed, missing required fields."]
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        if not new_user_dict["password1"] == new_user_dict["password2"]:
            # Passwords don't match. 
            d = {}
            d["error_msgs"] = ["User creation failed, passwords don't match."]
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        if not password_is_strong(new_user_dict["password1"]):
            # Password isn't strong enough. 
            d = {}
            d["error_msgs"] = ["User creation failed, passwords is too weak. Increase it's complexity."]
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        new_user = User(email = new_user_dict["email"], full_name = new_user_dict["name"], cleartext_password = new_user_dict["password1"])

        # Optional fields.
        try:
            new_user.permissions = new_user_dict["permissions"]
        except KeyError:
            new_user.permissions = ""

        try:
            new_user.is_admin = new_user_dict["is_admin"]
        except KeyError:
            new_user.is_admin = False

        try:
            db.session.add(new_user)
            db.session.commit()
            return api_user(new_user.id)
        except:
            d = {}
            d["error_msgs"] = ["User creation failed, commit to database was unsuccessful."]
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

# REST interface to the given user
@app.route("/api/users/<user_id>", methods=["GET", "PUT", "DELETE"])
def api_user(user_id):
    try:
        require_login()
    except AuthenticationException:
        d = {}
        d["error_msgs"] = ["You must be authenticated."]
        resp = flask.Response(json.dumps(d), status = 401)
        resp.mimetype = "application/json"
        return resp
    except AuthorizationException:
        d = {}
        d["error_msgs"] = ["You do not have permission to access that."]
        app.logger.warning("AuthorizationException: User '%s' tried to access user ID '%s'." % (flask.session["email"], user_id))
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    if user_id == "me":
        user_id = flask.session["user_id"]

    user_id = int(user_id)
    # Check to see if the user is either the owner of the account being accessed, or that they are an admin (only admins can access the account details of another user)
    if not user_id == flask.session["user_id"] and not flask.session["is_admin"]:
        d = {}
        d["error_msgs"] = ["You do not have permission to access that."]
        app.logger.warning("AuthorizationException: User '%s' tried to access user ID '%s'." % (flask.session["email"], user_id))
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    # Get object referred to by ID
    user_obj = db.session.query(User).filter_by(id = user_id).first()

    if user_obj == None:
        # Object was not found.
        d = {}
        d["error_msgs"] = ["User does not exist."]
        resp = flask.Response(json.dumps(d), status = 404)
        resp.mimetype = "application/json"
        return resp

    if flask.request.method == "GET":
        # We don't need to do anything else here
        pass
    elif flask.request.method == "PUT":
        user_dict = flask.request.get_json()

        if user_dict["name"]:
            user_obj.full_name = user_dict["name"]

        if user_dict["email"]:
            user_obj.email = user_dict["email"]

        if user_dict["theme"]:
            user_obj.theme = user_dict["theme"]

        # Only admins can change any users admin status and permissions
        if not user_dict["is_admin"] == user_obj.is_admin:
            if not flask.session["is_admin"]:
                d = {}
                d["error_msgs"] = ["You do not have permission to do that."]
                app.logger.warning("AuthorizationException: User '%s' tried to change user ID '%s' administrator permissions." % (flask.session["email"], user_id))
                resp = flask.Response(json.dumps(d), status = 403)
                resp.mimetype = "application/json"
                return resp

            user_obj.is_admin = user_dict["is_admin"]

        try:
            if user_dict["permissions"] != user_obj.permissions:
                if not flask.session["is_admin"]:
                    d = {}
                    d["error_msgs"] = ["You do not have permission to do that."]
                    app.logger.warning("AuthorizationException: User '%s' tried to change user ID '%s' permissions." % (flask.session["email"], user_id))
                    resp = flask.Response(json.dumps(d), status = 403)
                    resp.mimetype = "application/json"
                    return resp
                user_obj.permissions = user_dict["permissions"]
        except KeyError:
            pass


        if user_dict["password1"] or user_dict["password2"]:
            if not user_dict["password1"] == user_dict["password2"]:
                # Passwords don't match. 
                d = {}
                d["error_msgs"] = ["User update failed, passwords don't match."]
                resp = flask.Response(json.dumps(d), status = 400)
                resp.mimetype = "application/json"
                return resp
            else:
                user_obj.set_password(user_dict["password1"])

        db.session.commit()

    elif flask.request.method == "DELETE":

        # Only admins can delete anything. Otherwise users could delete themselves.
        if not flask.session["is_admin"]:
            d = {}
            d["error_msgs"] = ["You do not have permission to do that."]
            app.logger.warning("AuthorizationException: User '%s' tried to delete user ID '%s'." % (flask.session["email"], user_id))
            resp = flask.Response(json.dumps(d), status = 403)
            resp.mimetype = "application/json"
            return resp
        tmp_username = user_obj.full_name
        db.session.delete(user_obj)
        db.session.commit()

        # Success
        d = {}
        d["success_msg"] = "User '%s' deleted." % tmp_username
        d["redirect"] = "/"
        resp = flask.Response(json.dumps(d), status = 200)
        resp.mimetype = "application/json"
        return resp

    resp = flask.Response(json.dumps(user_obj.to_dict()), status = 200)

    # This is special code to automatically update the JWT if we made changes to ourselves. Basically just replaces the cookie.
    if flask.request.method == "PUT" and user_id == flask.session["user_id"]:
        flask.session["user_id"] = user_obj.id
        flask.session["is_admin"] = user_obj.is_admin
        flask.session["email"] = user_obj.email
        resp.set_cookie("__theme", user_obj.theme, max_age=60*60*24*7, httponly=False, secure=flask.request.scheme == "https")

    resp.mimetype = "application/json"
    return resp


@app.route("/api/shortcuts", methods=["GET", "POST"])
def api_shortcuts():
    try:
        require_login()
    except AuthenticationException:
        d = {}
        d["error_msgs"] = ["You must be authenticated."]
        resp = flask.Response(json.dumps(d), status = 401)
        resp.mimetype = "application/json"
        return resp
    except AuthorizationException:
        d = {}
        d["error_msgs"] = ["You do not have permission to access that."]
        app.logger.warning("AuthorizationException: User '%s' tried to access the shortcuts list." % (flask.session["email"]))
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    if flask.request.method == "GET":
        d = {}
        d["error_msgs"] = []
        errors = False

        # Get a full listing of shortcuts owned by this user
        shortcuts_list = []
        try:
            shortcuts_objs = db.session.query(Shortcut).filter_by(owner_id = flask.session["user_id"])
            for shortcut in shortcuts_objs:
                shortcuts_list.append(shortcut.to_dict())
            resp = flask.Response(json.dumps(shortcuts_list), status = 200)
            resp.mimetype = "application/json"
            return resp
        except:
            d["error_msgs"].append("Failed to retrieve shortcuts.")
            resp = flask.Response(json.dumps(d), status = 500)
            resp.mimetype = "application/json"
            return resp
        
    if flask.request.method == "POST":
        d = {}
        d["error_msgs"] = []
        errors = False

        try:
            ensure_json(flask.request)
        except ContentException:
            d = {}
            d["error_msgs"] = ["Incorrect content type."]
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        try:
            new_shortcut_dict = flask.request.get_json()

            try:
                new_shortcut_dict["keyword"]
                if new_shortcut_dict["keyword"] == "":
                    raise KeyError
            except KeyError:
                d["error_msgs"].append("Failed to create new shortcut. Missing 'keyword' parameter.")
                errors = True
                
            try:
                new_shortcut_dict["url"]
                if new_shortcut_dict["keyword"] == "":
                    raise KeyError
            except KeyError:
                d["error_msgs"].append("Failed to create new shortcut. Missing 'url' parameter.")
                errors = True

            if errors:
                raise StartServiceException

            new_shortcut = Shortcut(owner_id = flask.session["user_id"], keyword = new_shortcut_dict["keyword"], url = new_shortcut_dict["url"])

            try:
                # Description is optional.
                new_shortcut.description = new_shortcut_dict["description"]
            except KeyError:
                pass

            try:
                db.session.add(new_shortcut)
                db.session.commit()
                return api_shortcut(new_shortcut.id)

            except:
                d["error_msgs"].append("Failed to create new shortcut. Database session did not accept object.")
                raise StartServiceException

        except StartServiceException:
            resp = flask.Response(json.dumps(d), status = 500)
            resp.mimetype = "application/json"
            return resp

# REST interface to the given shortcut
@app.route("/api/shortcuts/<int:id>", methods=["GET", "PUT", "DELETE"])
def api_shortcut(id):
    try:
        require_login()
    except AuthenticationException:
        d = {}
        d["error_msgs"] = ["You must be authenticated."]
        resp = flask.Response(json.dumps(d), status = 401)
        resp.mimetype = "application/json"
        return resp
    except AuthorizationException:
        d = {}
        d["error_msgs"] = ["You do not have permission to access that."]
        app.logger.warning("AuthorizationException: User '%s' tried to access shortcut ID '%d'." % (flask.session["email"], id))
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    # Get object referred to by ID
    shortcut_obj = db.session.query(Shortcut).filter_by(owner_id = flask.session["user_id"]).filter_by(id = id).first()

    if shortcut_obj == None:
        # Object was not found.
        d = {}
        d["error_msgs"] = []
        d["error_msgs"].append("Object not found.")
        resp = flask.Response(json.dumps(d), status = 404)
        resp.mimetype = "application/json"
        return resp

    if flask.request.method == "GET":
        resp = flask.Response(json.dumps(shortcut_obj.to_dict()), status = 200)
        resp.mimetype = "application/json"
        return resp

    elif flask.request.method == "PUT":
        d = {}
        d["error_msgs"] = []
        errors = False

        try:
            shortcut_dict = flask.request.get_json()

            try:
                shortcut_dict["keyword"]
                if shortcut_dict["keyword"] == "":
                    raise KeyError
            except KeyError:
                d["error_msgs"].append("Failed to update shortcut. The 'keyword' parameter is required.")
                errors = True

            try:
                shortcut_dict["url"]
                if shortcut_dict["url"] == "":
                    raise KeyError
            except KeyError:
                d["error_msgs"].append("Failed to update shortcut. The 'url' parameter is required.")
                errors = True

            if errors:
                raise StartServiceException

            try:
                shortcut_obj.keyword = shortcut_dict["keyword"]
                shortcut_obj.url = shortcut_dict["url"]
                try:
                    shortcut_obj.description = shortcut_dict["description"]
                except KeyError:
                    shortcut_obj.description = ""

                db.session.commit()

                resp = flask.Response(json.dumps(shortcut_obj.to_dict()), status = 200)
                resp.mimetype = "application/json"
                return resp

            except:
                d["error_msgs"].append("Failed to update shortcut. The database rejected the update.")
                raise StartServiceException

        except StartServiceException:
            resp = flask.Response(json.dumps(d), status = 500)
            resp.mimetype = "application/json"
            return resp

    elif flask.request.method == "DELETE":
        d = {}
        d["error_msgs"] = []
        try:
            tmp_shortcut_name = shortcut_obj.keyword
            db.session.delete(shortcut_obj)
            db.session.commit()

            # Success
            d["success_msgs"] = []
            d["success_msgs"].append("Shortcut '%s' deleted." % tmp_shortcut_name)
            resp = flask.Response(json.dumps(d), status = 200)
            resp.mimetype = "application/json"
            return resp

        except StartServiceException:
            d["error_msgs"].append("Failed to delete shortcut. The database rejected the deletion.")
            resp = flask.Response(json.dumps(d), status = 500)
            resp.mimetype = "application/json"
            return resp

    resp = flask.Response(json.dumps(shortcut_obj.to_dict()), status = 200)
    resp.mimetype = "application/json"
    return resp

# Error Handlers    
@app.errorhandler(404)
def error_not_found_handler(e):
    return flask.redirect("/#!/error/not-found")

# In theory, this only gets thrown on API calls.
@app.errorhandler(500)
def error_not_found_handler(e):
    d = {}
    d["error_msgs"] = ["Unexpected server error. Something went wrong."]
    app.logger.error(e)
    resp = flask.Response(json.dumps(d), status = 500)
    resp.mimetype = "application/json"
    return resp

if __name__ == "__main__":
    app.run()
